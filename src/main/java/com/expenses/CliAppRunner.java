package com.expenses;

import com.expenses.action.ActionContext;
import com.expenses.currency.CurrencyService;
import com.expenses.currency.OfflineCurrencyService;
import com.expenses.io.ExpenseFileMapper;
import com.expenses.io.FileType;

import java.io.PrintStream;
import java.util.Scanner;

public class CliAppRunner {
  public static void main(String[] args) {
    PrintStream printStream = System.out;
    Scanner scanner = new Scanner(System.in);
    CurrencyService currencyService = new OfflineCurrencyService();
    ExpenseService expenseService = new ExpenseService(currencyService);
    ActionContext actionContext = new ActionContext(scanner, printStream, expenseService, currencyService, new ExpenseFileMapper());

    ExpenseCliApp application = new ExpenseCliApp(actionContext);
    application.run("data.csv", FileType.CSV);
  }
}
