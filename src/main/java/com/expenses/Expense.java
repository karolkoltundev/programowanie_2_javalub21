package com.expenses;

import com.expenses.currency.Currency;
import com.expenses.util.NumberHelper;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Expense {
  private final BigDecimal amount;
  private final Currency currency;
  private final LocalDate date;
  private final String location;
  private final String category;

  private Expense(BigDecimal amount,
                  LocalDate date,
                  String location,
                  String category) {
    this(amount, Currency.POLISH_ZLOTY, date, location, category);
  }

  private Expense(BigDecimal amount,
                  Currency currency,
                  LocalDate date,
                  String location,
                  String category) {
    this.amount = amount;
    this.currency = currency;
    this.date = date;
    this.location = location;
    this.category = category;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public Currency getCurrency() {
    return currency;
  }

  public LocalDate getDate() {
    return date;
  }

  public String getLocation() {
    return location;
  }

  public String getCategory() {
    return category;
  }

  @Override
  public String toString() {
    DateTimeFormatter dateFormat = DateTimeFormatter
        .ofPattern("dd-MM-yyyy");
    String message = "{%s, %s %s, %s, %s}";

    return String.format(message,
        date.format(dateFormat), amount, currency, location, category);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Expense expense = (Expense) o;
    return Objects.equals(amount, expense.amount) &&
        Objects.equals(date, expense.date) &&
        Objects.equals(location, expense.location) &&
        Objects.equals(category, expense.category);
  }

  @Override
  public int hashCode() {
    return Objects.hash(amount, date, location, category);
  }

  @JsonCreator
  public static Expense from(@JsonProperty("amount") BigDecimal amount,
                             @JsonProperty("date") LocalDate date,
                             @JsonProperty("location") String location,
                             @JsonProperty("category") String category)
      throws InvalidExpenseException{
    return from(amount, Currency.POLISH_ZLOTY, date, location, category);
  }

  public static Expense convertedFrom(Expense expense, BigDecimal amount, Currency currency) {
    return new Expense(amount, currency, expense.getDate(), expense.getLocation(), expense.getCategory());
  }

  public static Expense from(BigDecimal amount,
                             Currency currency,
                             LocalDate date,
                             String location,
                             String category)
      throws InvalidExpenseException{
    if (amount.compareTo(BigDecimal.ZERO) < 0) {
      throw new InvalidExpenseException();
    }
    if (date == null || date.isAfter(LocalDate.now())) {
      throw new InvalidExpenseException();
    }
    if (location == null || location.isBlank()) {
      throw new InvalidExpenseException();
    }
    if (!NumberHelper.hasTwoOrLessDecimalPlaces(amount.toPlainString())) {
      throw new InvalidExpenseException();
    }
    return new Expense(amount, currency, date, location, category);
  }
}
