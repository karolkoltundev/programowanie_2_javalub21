package com.expenses;

import com.expenses.action.Action;
import com.expenses.action.ActionContext;
import com.expenses.action.ActionFactory;
import com.expenses.action.MenuAction;
import com.expenses.io.ExpenseFileMapper;
import com.expenses.io.FileType;

import java.io.IOException;
import java.util.Set;

public class ExpenseCliApp {

  private final ActionContext actionContext;

  public ExpenseCliApp(ActionContext actionContext) {
    this.actionContext = actionContext;
  }

  public void run(String dataFilename, FileType fileType) {
    loadData(dataFilename, fileType);
    run();
    saveData(dataFilename, fileType);
  }

  public void run() {
    actionContext.printStream().println("Hello User!");

    while (!actionContext.isShuttingDown()) {
      MenuAction menuAction = actionContext.popCurrentMenu();
      menuAction.execute(actionContext);
    }

    actionContext.printStream().println("Goodbye!");
  }

  private void loadData(String filename, FileType fileType) {
    actionContext.printStream().println("Loading data from " + filename);

    Set<Expense> expenses;
    try {
      ExpenseFileMapper expenseFileMapper = new ExpenseFileMapper();
      expenses = expenseFileMapper.readFromFile(filename, fileType);
    } catch (IOException exception) {
      actionContext.printStream().println("Could not load data from " + filename + ": " + exception.getMessage());
      return;
    }

    ExpenseService expenseService = actionContext.expenseService();
    expenses.forEach(expenseService::addExpense);
    actionContext.printStream().println("Successfully loaded " + expenses.size() + " expenses.");
  }

  private void saveData(String filename, FileType fileType) {
    actionContext.printStream().println("Saving data to " + filename);

    try {
      ExpenseFileMapper expenseFileMapper = new ExpenseFileMapper();
      expenseFileMapper.writeToFile(filename, fileType, actionContext.expenseService().getExpenses());
    } catch (IOException exception) {
      actionContext.printStream().println("Could not save data to " + filename + ": " + exception.getMessage());
    }
  }
}
