package com.expenses.io;

public class ExpenseMapperFactory {
  enum FileType {
    CSV,
    TSV,
    JSON
  }

  static ExpenseMapper getMapper(FileType fileType) {
    switch (fileType) {
      case CSV:
        return new ExpenseCsvMapper();
      case TSV:
        return new ExpenseCsvMapper("\t");
      case JSON:
        return new ExpenseJsonMapper();
      default:
        throw new RuntimeException("Unknown filetype!");
    }
  }
}
