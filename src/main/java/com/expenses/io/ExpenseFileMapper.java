package com.expenses.io;

import com.expenses.Expense;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class ExpenseFileMapper {
  public void writeToFile(String filename, Set<Expense> expenses) throws IOException {
    // todo try to guess filetype from filename
    // FileType fileType = guessed filetype
    // writeToFile(filename, fileType, expenses);
  }

  public void writeToFile(String filename, FileType fileType, Set<Expense> expenses) throws IOException {
    ExpenseMapper expenseMapper = fileType.getMapper();

    try (FileWriter fileWriter = new FileWriter(filename)) {
      expenseMapper.write(expenses, fileWriter);
    }
  }

  public Set<Expense> readFromFile(String filename) throws IOException {
    // todo try to guess filetype from filename
    // FileType fileType = guessed filetype
    // return readFromFile(filename, fileType);

    // Not implemented yet
    return new HashSet<>();
  }

  public Set<Expense> readFromFile(String filename, FileType fileType) throws IOException {
    // todo try to guess filetype from filename instead of getting it as an argument
    ExpenseMapper expenseMapper = fileType.getMapper();

    try (FileReader fileReader = new FileReader(filename)) {
      return expenseMapper.read(fileReader);
    }
  }
}
