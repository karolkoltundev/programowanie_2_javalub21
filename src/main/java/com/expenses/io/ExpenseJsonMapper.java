package com.expenses.io;

import com.expenses.Expense;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Set;

public class ExpenseJsonMapper implements ExpenseMapper {
  private final ObjectMapper objectMapper;

  public ExpenseJsonMapper() {
    objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
    objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
  }

  @Override
  public Set<Expense> read(Reader reader) throws IOException {
    TypeReference<Set<Expense>> setOfExpenses = new TypeReference<>() {};
    return objectMapper.readValue(reader, setOfExpenses);
  }

  @Override
  public void write(Set<Expense> expenses, Writer writer) throws IOException {
    objectMapper.writeValue(writer, expenses);
  }
}
