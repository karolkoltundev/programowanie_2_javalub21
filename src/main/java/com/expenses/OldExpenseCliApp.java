package com.expenses;

import com.expenses.currency.Currency;
import com.expenses.currency.CurrencyService;
import com.expenses.io.ExpenseFileMapper;
import com.expenses.io.FileType;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class OldExpenseCliApp {

  private final Scanner scanner;
  private final PrintStream printStream;
  private final ExpenseService expenseService;
  private final CurrencyService currencyService;

  public OldExpenseCliApp(InputStream inputStream,
                          PrintStream printStream,
                          ExpenseService expenseService,
                          CurrencyService currencyService) {
    this.scanner = new Scanner(inputStream);
    this.printStream = printStream;
    this.expenseService = expenseService;
    this.currencyService = currencyService;
  }

  public void run(String dataFilename, FileType fileType) {
    loadData(dataFilename, fileType);
    run();
    saveData(dataFilename, fileType);
  }

  public void run() {
    printStream.println("Hello User!");

    boolean shutdownChosen = false;
    while (!shutdownChosen) {
      printStream.println("MAIN MENU");
      printStream.println("x - exit - application shutdown");
      printStream.println("a - add - add new expense");
      printStream.println("l - list - list expenses");
      printStream.println("c - convert - convert amount to PLN");

      String option = scanner.nextLine();
      if (option.equals("x") || option.equals("exit")) {
        printStream.println("Shutting down");
        shutdownChosen = true;
      } else if (option.equals("a") || option.equals("add")) {
        tryToAddExpense();
      } else if (option.equals("l") || option.equals("list")) {
        listExpenses();
      } else if (option.equals("c") || option.equals("convert")) {
        convertAmount();
      }
    }

    printStream.println("Goodbye!");
  }

  private void tryToAddExpense() {
    printStream.println("Adding new expense");

    printStream.println("Enter date in dd-MM-yyyy format:");
    LocalDate date;
    String enteredDate = scanner.nextLine();
    try {
      date = LocalDate.parse(enteredDate, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    } catch (DateTimeParseException exception) {
      printStream.println("Entered date " + enteredDate + " is invalid.");
      return;
    }

    printStream.println("Enter amount:");
    BigDecimal amount;
    String enteredAmount = scanner.nextLine();
    try {
      amount = new BigDecimal(enteredAmount);
    } catch (NumberFormatException exception) {
      printStream.println("Entered amount " + enteredAmount + " is invalid.");
      return;
    }

    printStream.println("Enter currency code:");
    String availableCodesMessage = Arrays.stream(Currency.values())
        .map(Currency::getCurrencyCode)
        .collect(Collectors.joining(", ", "Available codes: ", "."));
    printStream.println(availableCodesMessage);

    String chosenCurrency = scanner.nextLine();
    Currency currency;
    if (StringUtils.isBlank(chosenCurrency)) {
      currency = Currency.POLISH_ZLOTY;
    } else {
      Optional<Currency> parsedCurrency = Currency.findByCode(chosenCurrency);
      if (parsedCurrency.isEmpty()) {
        printStream.println("Currency code [" + chosenCurrency + "] is unknown.");
        return;
      } else {
        currency = parsedCurrency.get();
      }
    }

    printStream.println("Enter non-empty location:");
    String location = scanner.nextLine();
    if (StringUtils.isBlank(location)) {
      printStream.println("Entered location is blank.");
      return;
    }

    printStream.println("Enter category (can by empty):");
    String category = scanner.nextLine();

    Expense expense;
    try {
      expense = Expense.from(amount, currency, date, location, category);
    } catch (InvalidExpenseException exception) {
      String message = "Could not create expense: "
          + (StringUtils.isBlank(exception.getMessage())
              ? "Unknown error."
              : exception.getMessage());
      printStream.println(message);
      return;
    }

    printStream.println("Adding expense: " + expense.toString());
    expenseService.addExpense(expense);
  }

  private void listExpenses() {
    Consumer<Expense> expensePrinterAnonClassObj = new Consumer<Expense>() {
      @Override
      public void accept(Expense expense) {
        printStream.println(expense);
      }
    };
    Consumer<Expense> expensePrinterLambda = (expense) -> printStream.println(expense);
    Consumer<Expense> expensePrinterMethodRef = printStream::println;

    Set<Expense> expenses = expenseService.getExpenses();
    printStream.println("Listing expenses");
    expenses.forEach(printStream::println);
  }

  private void convertAmount() {
    printStream.println("Enter amount:");
    BigDecimal amount;
    String enteredAmount = scanner.nextLine();
    try {
      amount = new BigDecimal(enteredAmount);
    } catch (NumberFormatException exception) {
      printStream.println("Entered amount " + enteredAmount + " is invalid.");
      return;
    }

    printStream.println("Enter currency code:");
    String availableCurrencies = Arrays.stream(Currency.values())
        .map(Currency::getCurrencyCode)
        .collect(Collectors.joining(", "));
    printStream.println("Available codes: [" + availableCurrencies + "]");

    String enteredCode = scanner.nextLine();
    Optional<Currency> chosenCurrency = Currency.findByCode(enteredCode);
    if (chosenCurrency.isEmpty()) {
      printStream.println("Unknown currency code: [" + enteredCode + "]");
      return;
    }

    BigDecimal convertedAmount = currencyService.convertToPln(amount, chosenCurrency.get());
    printStream.println(amount + " " + enteredCode + " = "
        + convertedAmount + " " + Currency.POLISH_ZLOTY.getCurrencyCode());
  }

  private void loadData(String filename, FileType fileType) {
    printStream.println("Loading data from " + filename);

    Set<Expense> expenses;
    try {
      ExpenseFileMapper expenseFileMapper = new ExpenseFileMapper();
      expenses = expenseFileMapper.readFromFile(filename, fileType);
    } catch (IOException exception) {
      printStream.println("Could not load data from " + filename + ": " + exception.getMessage());
      return;
    }

    expenses.forEach(expenseService::addExpense);
    printStream.println("Successfully loaded " + expenses.size() + " expenses.");
  }

  private void saveData(String filename, FileType fileType) {
    printStream.println("Saving data to " + filename);

    try {
      ExpenseFileMapper expenseFileMapper = new ExpenseFileMapper();
      expenseFileMapper.writeToFile(filename, fileType, expenseService.getExpenses());
    } catch (IOException exception) {
      printStream.println("Could not save data to " + filename + ": " + exception.getMessage());
    }
  }
}
