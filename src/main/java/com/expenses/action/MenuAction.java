package com.expenses.action;

import java.util.List;
import java.util.Optional;

public class MenuAction implements Action {

  private final String title;
  private final List<MenuOption> options;

  public MenuAction(String title,
                    List<MenuOption> options) {
    this.title = title;
    this.options = options;
  }

  public String getTitle() {
    return title;
  }

  @Override
  public void execute(ActionContext actionContext) {
    actionContext.setCurrentMenu(this);

    actionContext.printStream().println(actionContext.getMenuStack());

    for (MenuOption option : options) {
      String optionMessage = String.format("[%s] [%s] %s",
          option.getShortCommand(),
          option.getLongCommand(),
          option.getActionType());
      actionContext.printStream().println(optionMessage);
    }

    String userInput = actionContext.scanner().nextLine();

    Optional<ActionFactory.ActionType> chosenAction = Optional.empty();
    if (userInput.length() == 1) {
      char shortCommand = userInput.charAt(0);

      for (MenuOption option : options) {
        if (option.getShortCommand() == shortCommand) {
          chosenAction = Optional.of(option.getActionType());
          break;
        }
      }
    } else {
      for (MenuOption option : options) {
        if (option.getLongCommand().equals(userInput)) {
          chosenAction = Optional.of(option.getActionType());
          break;
        }
      }
    }

    if (chosenAction.isEmpty()) {
      actionContext.printStream().println("Unknown option: " + userInput);
    } else {
      ActionFactory.ActionType chosenActionType = chosenAction.get();
      ActionFactory.getAction(chosenActionType).execute(actionContext);
    }
  }
}
