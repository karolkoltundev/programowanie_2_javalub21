package com.expenses.action;

public class BackAction implements Action {
  @Override
  public void execute(ActionContext actionContext) {
    actionContext.popCurrentMenu();
  }
}
