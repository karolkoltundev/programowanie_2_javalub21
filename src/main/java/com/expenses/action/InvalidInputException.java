package com.expenses.action;

public class InvalidInputException extends Exception {
  public InvalidInputException(String message) {
    super(message);
  }
}
