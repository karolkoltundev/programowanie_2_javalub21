package com.expenses.action;

import com.expenses.Expense;
import com.expenses.InvalidExpenseException;
import com.expenses.currency.Currency;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;

public class AddExpenseAction extends AbstractInputAction {
  public void execute(ActionContext actionContext) {
    try {
      actionContext.printStream().println("Adding new expense");

      LocalDate date = getDate(actionContext);
      BigDecimal amount = getAmount(actionContext);
      Currency currency = getCurrency(actionContext);
      String location = getText(actionContext, "location", true);
      String category = getText(actionContext, "category", false);

      Expense expense = Expense.from(amount, currency, date, location, category);

      actionContext.printStream().println("Adding expense: " + expense.toString());

      actionContext.expenseService().addExpense(expense);

    } catch (InvalidInputException exception) {
      actionContext.printStream().println(exception.getMessage());
    } catch (InvalidExpenseException exception) {
      String message = "Could not create expense: "
          + (StringUtils.isBlank(exception.getMessage())
          ? "Unknown error."
          : exception.getMessage());
      actionContext.printStream().println(message);
    }
  }
}
