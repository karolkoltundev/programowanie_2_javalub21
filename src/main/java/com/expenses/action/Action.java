package com.expenses.action;

public interface Action {
  void execute(ActionContext actionContext);
}
