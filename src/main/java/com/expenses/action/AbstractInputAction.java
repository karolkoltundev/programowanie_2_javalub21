package com.expenses.action;

import com.expenses.currency.Currency;

import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class AbstractInputAction implements Action {
  protected BigDecimal getAmount(ActionContext actionContext) throws InvalidInputException {
    actionContext.printStream().println("Enter amount:");
    BigDecimal amount;
    String enteredAmount = actionContext.scanner().nextLine();
    try {
      amount = new BigDecimal(enteredAmount);
    } catch (NumberFormatException exception) {
      throw new InvalidInputException("Entered amount " + enteredAmount + " is invalid.");
    }
    return amount;
  }

  protected LocalDate getDate(ActionContext actionContext) throws InvalidInputException {
    // todo
    return null;
  }

  protected Currency getCurrency(ActionContext actionContext) throws InvalidInputException {
    // todo
    return null;
  }

  // getting non-optional location: getText(actionContext, "location", true);
  // getting optional category: getText(actionContext, "category", false);
  protected String getText(ActionContext actionContext, String textName, boolean required) {
    // todo
    return null;
  }
}
