package com.expenses.action;

public class ListExpensesAction implements Action {
  public void execute(ActionContext actionContext) {
    actionContext.expenseService()
        .getExpenses()
        .forEach(expense -> actionContext.printStream().println(expense));
  }
}
