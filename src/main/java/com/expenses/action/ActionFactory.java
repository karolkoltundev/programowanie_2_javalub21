package com.expenses.action;

import java.util.List;

public class ActionFactory {
  public enum ActionType {
    ADD_EXPENSE {
      @Override
      Action createAction() {
        return new AddExpenseAction();
      }
    },
    LIST_EXPENSES {
      @Override
      Action createAction() {
        return new ListExpensesAction();
      }
    },
    EXPENSE_MENU {
      @Override
      Action createAction() {
        List<MenuOption> expenseMenuOptions = List.of(
            new MenuOption('a', "add", ActionType.ADD_EXPENSE),
            new MenuOption('l', "list", ActionType.LIST_EXPENSES),
            new MenuOption('b', "back", ActionType.BACK_ACTION),
            new MenuOption('s', "submenu", ActionType.EXAMPLE_SUBMENU)
        );
        return new MenuAction("Expense menu", expenseMenuOptions);
      }
    },
    CURRENCY_MENU {
      @Override
      Action createAction() {
        List<MenuOption> expenseMenuOptions = List.of(
            new MenuOption('e', "exchange", ActionType.EXCHANGE_ACTION),
            new MenuOption('r', "rates", ActionType.RATES_ACTION),
            new MenuOption('b', "back", ActionType.BACK_ACTION)
        );
        return new MenuAction("Currency menu", expenseMenuOptions);
      }
    },
    EXAMPLE_SUBMENU {
      @Override
      Action createAction() {
        List<MenuOption> expenseMenuOptions = List.of(
            new MenuOption('h', "hello", ActionType.HELLO_ACTION),
            new MenuOption('a', "author", ActionType.AUTHOR_ACTION),
            new MenuOption('b', "back", ActionType.BACK_ACTION)
            );
        return new MenuAction("Example submenu", expenseMenuOptions);
      }
    },
    HELLO_ACTION {
      @Override
      Action createAction() {
        return actionContext -> actionContext.printStream().println("HELLO WORLD!");
      }
    },
    AUTHOR_ACTION {
      @Override
      Action createAction() {
        return actionContext -> actionContext.printStream().println("Karol Koltun");
      }
    },
    EXCHANGE_ACTION {
      @Override
      Action createAction() {
        return new ExchangeAction();
      }
    },
    RATES_ACTION {
      @Override
      Action createAction() {
        return new RatesAction();
      }
    },
    BACK_ACTION {
      @Override
      Action createAction() {
        return new BackAction();
      }
    },
    MAIN_MENU {
      @Override
      Action createAction() {
        return ActionType.getMainMenu();
      }
    },
    SHUTDOWN {
      @Override
      Action createAction() {
        return new ShutdownAction();
      }
    };

    abstract Action createAction();

    private static MenuAction getMainMenu() {
      List<MenuOption> expenseMenuOptions = List.of(
          new MenuOption('e', "expense", ActionType.EXPENSE_MENU),
          new MenuOption('c', "currency", ActionType.CURRENCY_MENU),
          new MenuOption('x', "exit", ActionType.SHUTDOWN)
      );
      return new MenuAction("Main menu", expenseMenuOptions);
    }
  }

  public static Action getAction(ActionType actionType) {
    return actionType.createAction();
  }

  static MenuAction getMainMenu() {
    return ActionType.getMainMenu();
  }
}
