package com.expenses.action;

import com.expenses.ExpenseService;
import com.expenses.currency.CurrencyService;
import com.expenses.io.ExpenseFileMapper;

import java.io.PrintStream;
import java.util.Scanner;
import java.util.Stack;
import java.util.stream.Collectors;

public class ActionContext {
  private boolean shuttingDown;
  private Stack<MenuAction> menuStack;

  private final Scanner scanner;
  private final PrintStream printStream;

  private final ExpenseService expenseService;
  private final CurrencyService currencyService;
  private final ExpenseFileMapper expenseFileMapper;

  public ActionContext(Scanner scanner,
                       PrintStream printStream,
                       ExpenseService expenseService,
                       CurrencyService currencyService,
                       ExpenseFileMapper expenseFileMapper) {
    this.shuttingDown = false;

    this.menuStack = new Stack<>();
    this.menuStack.add(ActionFactory.getMainMenu());

    this.scanner = scanner;
    this.printStream = printStream;
    this.expenseService = expenseService;
    this.currencyService = currencyService;
    this.expenseFileMapper = expenseFileMapper;
  }

  public MenuAction popCurrentMenu() {
    return menuStack.pop();
  }

  public void setCurrentMenu(MenuAction currentMenu) {
    this.menuStack.push(currentMenu);
  }

  public void startShutdown() {
    shuttingDown = true;
  }

  public boolean isShuttingDown() {
    return shuttingDown || menuStack.isEmpty();
  }

  public String getMenuStack() {
    return menuStack.stream()
        .map(MenuAction::getTitle)
        .collect(Collectors.joining(" | "));
  }

  public Scanner scanner() {
    return scanner;
  }

  public PrintStream printStream() {
    return printStream;
  }

  public ExpenseService expenseService() {
    return expenseService;
  }

  public CurrencyService currencyService() {
    return currencyService;
  }

  public ExpenseFileMapper expenseFileMapper() {
    return expenseFileMapper;
  }
}
