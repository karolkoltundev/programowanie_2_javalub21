package com.expenses.action;

import static com.expenses.action.ActionFactory.ActionType.*;

public class ExpenseMenuAction implements Action {
  @Override
  public void execute(ActionContext actionContext) {
    actionContext.printStream().println("Expense menu:");
    actionContext.printStream().println("a - add - add new expense");
    actionContext.printStream().println("l - list - list expenses");
    actionContext.printStream().println("x - exit - application shutdown");

    String option = actionContext.scanner().nextLine();

    Action action;
    switch (option) {
      case "x":
      case "exit":
        action = ActionFactory.getAction(SHUTDOWN);
        break;
      case "a":
      case "add":
        action = ActionFactory.getAction(ADD_EXPENSE);
        break;
      case "l":
      case "list":
        action = ActionFactory.getAction(LIST_EXPENSES);
        break;
      default:
        actionContext.printStream().println("Unknown action: [" + option + "]");
        return;
    }

    action.execute(actionContext);
  }
}
