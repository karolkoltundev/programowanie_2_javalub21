package com.expenses.action;

public class MenuOption {
  private final char shortCommand;
  private final String longCommand;
  private final ActionFactory.ActionType actionType;

  public MenuOption(char shortCommand,
                    String longCommand,
                    ActionFactory.ActionType actionType) {
    this.shortCommand = shortCommand;
    this.longCommand = longCommand;
    this.actionType = actionType;
  }

  public char getShortCommand() {
    return shortCommand;
  }

  public String getLongCommand() {
    return longCommand;
  }

  public ActionFactory.ActionType getActionType() {
    return actionType;
  }
}
