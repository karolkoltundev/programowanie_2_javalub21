package com.expenses.action;

public class ShutdownAction implements Action {
  @Override
  public void execute(ActionContext actionContext) {
    actionContext.startShutdown();
  }
}
