package com.expenses.currency.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDate;

public class NbpApiRateDto {
  private String no;
  private LocalDate effectiveDate;
  @JsonProperty("mid")
  private BigDecimal averageRate;

  public NbpApiRateDto() {
  }

  public NbpApiRateDto(String no,
                       LocalDate effectiveDate,
                       BigDecimal averageRate) {
    this.no = no;
    this.effectiveDate = effectiveDate;
    this.averageRate = averageRate;
  }

  public String getNo() {
    return no;
  }

  public LocalDate getEffectiveDate() {
    return effectiveDate;
  }

  public BigDecimal getAverageRate() {
    return averageRate;
  }
}
