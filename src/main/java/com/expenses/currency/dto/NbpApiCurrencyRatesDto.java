package com.expenses.currency.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class NbpApiCurrencyRatesDto {
  private String table;
  @JsonProperty("currency")
  private String currencyName;
  @JsonProperty("code")
  private String currencyCode;
  private List<NbpApiRateDto> rates;

  public NbpApiCurrencyRatesDto() {
  }

  public NbpApiCurrencyRatesDto(String table,
                                String currencyName,
                                String currencyCode,
                                List<NbpApiRateDto> rates) {
    this.table = table;
    this.currencyName = currencyName;
    this.currencyCode = currencyCode;
    this.rates = rates;
  }

  public String getTable() {
    return table;
  }

  public String getCurrencyName() {
    return currencyName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public List<NbpApiRateDto> getRates() {
    return rates;
  }
}
