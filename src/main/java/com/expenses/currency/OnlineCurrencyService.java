package com.expenses.currency;

import com.expenses.currency.dto.NbpApiCurrencyRatesDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class OnlineCurrencyService implements CurrencyService {
  private static final String NBP_URI_PATTERN = "https://api.nbp.pl/api/exchangerates/rates/A/%s/last/1?format=json";

  private final ObjectMapper objectMapper;
  private final HttpClient httpClient;

  public OnlineCurrencyService() {
    objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());

    httpClient = HttpClient.newHttpClient();
  }

  @Override
  public BigDecimal convertToPln(BigDecimal amount, Currency currency) {
    NbpApiCurrencyRatesDto ratesDto = getRatesResponse(currency);

    if (ratesDto.getRates().isEmpty()) {
      throw new CurrencyConversionFailedException("Error getting rate from NBP API response - no rates found");
    }

    BigDecimal rate = ratesDto.getRates().get(0).getAverageRate();

    return amount.multiply(rate).setScale(2, RoundingMode.HALF_UP);
  }

  private NbpApiCurrencyRatesDto getRatesResponse(
      Currency currency) {
    HttpRequest httpRequest = HttpRequest.newBuilder()
        .uri(getUri(currency))
        .GET()
        .build();

    HttpResponse<String> response;
    try {
      response = httpClient.send(httpRequest,
          HttpResponse.BodyHandlers.ofString());
    } catch (Exception e) {
      throw new CurrencyConversionFailedException("Could not get answer from NBP API: " + e.getMessage());
    }

    String responseBody = response.body();

    NbpApiCurrencyRatesDto ratesDto;
    try {
      ratesDto = objectMapper.readValue(responseBody, NbpApiCurrencyRatesDto.class);
    } catch (IOException exception) {
      throw new CurrencyConversionFailedException("Could not read answer from NBP API: " + exception.getMessage());
    }

    return ratesDto;
  }

  private static URI getUri(Currency currency) {
    String currencyUri = String.format(NBP_URI_PATTERN, currency.getCurrencyCode());
    return URI.create(currencyUri);
  }
}
