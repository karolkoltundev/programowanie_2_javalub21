package com.interview.house;

public class House implements Comparable<House> {
  private int size;
  private int price;
  private String address;

  public House(int size, int price, String address) {
    this.size = size;
    this.price = price;
    this.address = address;
  }

  public int getSize() {
    return size;
  }

  public int getPrice() {
    return price;
  }

  public String getAddress() {
    return address;
  }

  @Override
  public int compareTo(House otherHouse) {
    return this.getPrice() - otherHouse.getPrice();
  }

  @Override
  public String toString() {
    return "House{" +
        "size=" + size +
        ", price=" + price +
        ", address='" + address + '\'' +
        '}';
  }
}
