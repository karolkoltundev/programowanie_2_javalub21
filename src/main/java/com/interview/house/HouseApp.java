package com.interview.house;

import java.util.*;
import java.util.stream.Collectors;

public class HouseApp {
  public static void main(String[] args) {
    List<House> houses = new ArrayList<>();
    houses.add(new House(110, 150, "Krasnickie"));
    houses.add(new House(100, 90, "Lipowa"));
    houses.add(new House(90, 150, "Lipowa"));
    houses.add(new House(350, 200, "Szerokie2"));
    houses.add(new House(120, 100, "Szerokie"));

    Set<House> sortedByPrice = houses.stream()
        .filter(h -> h.getSize() > 100)
        .sorted(House::compareTo)
        .collect(Collectors.toCollection(LinkedHashSet::new));

    for (House h : sortedByPrice) {
      System.out.println(h);
    }
  }
}
