package com.interview.algorythms;

public class Prime {
  public static boolean isPrime(int n) {
    if (n <= 3) {
      return n > 1;
    } else if (n % 2 == 0 || n % 3 == 0) {
      return false;
    }


    for (int i = 5; i*i <= n; i++) {
      if (n % i == 0) {
        return false;
      }
    }
    return true;
  }

  public static void main(String[] args) {
    System.out.println("14: " + isPrime(14));
    System.out.println("15: " + isPrime(15));
    System.out.println("17: " + isPrime(17));
    System.out.println("31: " + isPrime(31));
    System.out.println("6001: " + isPrime(6001));
    System.out.println("257: " + isPrime(257));
  }
}
