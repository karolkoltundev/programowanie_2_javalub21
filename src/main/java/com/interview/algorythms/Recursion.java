package com.interview.algorythms;

import java.util.Arrays;

public class Recursion {
  public static void main(String[] args) {
    int[] array = new int[101];
    arrayUntil(array, 100);

    Arrays.stream(array).forEach(System.out::println);
  }

  public static void arrayUntil(int[] array, int n) {
    if (n > 0) {
      arrayUntil(array, n -1);
    }
    array[n] = n;
  }
}
