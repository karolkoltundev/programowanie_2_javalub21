package com.interview.algorythms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MissingNumber {
  public static void amazonMissingNumberLukasz() {
    List<Integer> numbers = IntStream.rangeClosed(1, 52)
        .boxed()
        .collect(Collectors.toList());
    Collections.shuffle(numbers);

    int removedNumber = numbers.remove(numbers.size() - 1);
    System.out.println("Removed number: " + removedNumber);

    Set<Integer> presentNumbers = IntStream.rangeClosed(1, 52)
        .boxed()
        .collect(Collectors.toSet());

    for (int i : numbers) {
      presentNumbers.remove(i);
    }

      System.out.println("Missing number is " + presentNumbers.iterator().next());
  }

  public static void amazonMissingNumberOptimal() {
    List<Integer> numbers = IntStream.rangeClosed(1, 52)
        .boxed()
        .collect(Collectors.toList());
    Collections.shuffle(numbers);

    int removedNumber = numbers.remove(numbers.size() - 1);
    System.out.println("Removed number: " + removedNumber);

    int expectedSum = 52/2*(52 + 1);
    int actualSum = numbers.stream().reduce(Integer::sum).orElse(0);
    System.out.println("Missing number: " + (expectedSum - actualSum));
  }

  public static void main(String[] args) {
    amazonMissingNumberOptimal();
  }
}
