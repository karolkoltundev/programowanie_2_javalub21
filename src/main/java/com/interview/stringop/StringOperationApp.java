package com.interview.stringop;

public class StringOperationApp {
  public static void main(String[] args) {
    StringOperation repeat = text -> text.repeat(2);
    StringOperation upperCase = String::toUpperCase;

    System.out.println(repeat.modify("karol"));
  }
}
