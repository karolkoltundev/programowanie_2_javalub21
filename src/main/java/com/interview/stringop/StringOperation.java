package com.interview.stringop;

@FunctionalInterface
public interface StringOperation {
  String modify(String text);

  default String modifyTwice(String text) {
    String modifiedOnce = modify(text);
    String modifiedTwice = modify(modifiedOnce);

    return modifiedTwice;
  }
}
