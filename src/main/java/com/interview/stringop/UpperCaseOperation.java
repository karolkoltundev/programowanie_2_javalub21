package com.interview.stringop;

public class UpperCaseOperation implements StringOperation {
  @Override
  public String modify(String text) {
    return text.toUpperCase();
  }
}
