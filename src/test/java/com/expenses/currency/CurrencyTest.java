package com.expenses.currency;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyTest {
  @Test
  void shouldFindCurrencyByCode() {
    // Given
    String usdCode = "USD";
    Currency expectedCurrency = Currency.US_DOLLAR;

    // When
    Optional<Currency> actualCurrency = Currency.findByCode(usdCode);

    // Then
    assertTrue(actualCurrency.isPresent());
    assertEquals(expectedCurrency, actualCurrency.get());
  }
}