package com.expenses;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import java.util.Set;

import static java.math.BigDecimal.valueOf;
import static org.junit.jupiter.api.Assertions.*;

class ExpenseTest {

  @ParameterizedTest
  @ValueSource(doubles = {3.001, 3.322, 3.01000009, 3.02010})
  void shouldNotAllowAmountsWithMoreThan2DecimalPlaces(double amount) {
    // When
    Executable createExpenseWithInvalidAmount = () -> Expense.from(
        valueOf(amount),
        LocalDate.now(),
        "Loc",
        "Cat"
    );

    // Then
    assertThrows(InvalidExpenseException.class, createExpenseWithInvalidAmount);
  }

  @Test
  void shouldNotAllowExpensesInFuture() {
    // Given
    LocalDate dateInFuture = LocalDate.now().plusYears(1);

    // When
    Executable createExpenseInFuture = () -> Expense.from(
        valueOf(100),
        dateInFuture,
        "Location",
        "Category");

    // Then
    assertThrows(InvalidExpenseException.class, createExpenseInFuture);
  }

  @Test
  void shouldNotAllowNullDate() {
    // Given
    LocalDate nullDate = null;

    // When
    Executable createExpenseInFuture = () -> Expense.from(
        valueOf(100),
        nullDate,
        "Location",
        "Category");

    // Then
    assertThrows(InvalidExpenseException.class, createExpenseInFuture);
  }

  @Test
  void shouldReturnExpensesWithRequestedDate() throws InvalidExpenseException {
    // Given
    LocalDate requestedDate = LocalDate.now().minusDays(5);

    Expense expense1 = Expense.from(valueOf(100), requestedDate, "Loc", "Cat");
    Expense expense2 = Expense.from(valueOf(100), requestedDate.minusDays(10), "Loc", "Cat");
    Expense expense3 = Expense.from(valueOf(100), requestedDate, "Loc", "Cat");

    ExpenseService expenseService = new ExpenseService(null);
    expenseService.addExpense(expense1);
    expenseService.addExpense(expense2);
    expenseService.addExpense(expense3);

    // When
    Set<Expense> foundExpenses = expenseService.findByDate(requestedDate);

    // Then
    assertEquals(2, foundExpenses.size());
    assertTrue(foundExpenses.contains(expense1));
    assertTrue(foundExpenses.contains(expense3));
  }
}